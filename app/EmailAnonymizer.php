<?php

/**
 * Class EmailAnonymizer
 *
 * Sample usages (parametrized):
 *
 * aaa@aaa.com -> ...@aaa.com
 * aaa@aaa.aaa.com -> ...@aaa.aaa.com
 * a-a@a_a.com -> XXX@a_a.com
 */
class EmailAnonymizer implements Anonymizer
{
    /**
     * @var string
     */
    private $replacement;
    
    /**
     * PhoneNumberAnonymizer constructor.
     * @param string $replacement
     */
    public function __construct($replacement)
    {
        $this->replacement = $replacement;
    }
    
    /**
     * @param string $text
     * @return array of string
     */
    public function anonymize($text)
    {
        $fmr = explode(' ', $text);
        $count = count($fmr);
        for ($i = 0; $i < $count; $i++){
            preg_match('/\b[^\s]+@[^\s]+/', $fmr[$i], $match);
            if($match[0]){
                $email = explode('@',$fmr[$i]);
                $fmr[$i] = $this->replacement.'@'.$email[1];
            }

        }
        $text = implode(' ', $fmr);
        return $text;
    }
}